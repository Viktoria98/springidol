package ru.omsu.imit.springidol.poems;

public interface Poem {
    void recite();
}
