package ru.omsu.imit.springidol;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.omsu.imit.springidol.performers.PerformanceException;
import ru.omsu.imit.springidol.performers.Performer;

@SpringBootApplication
public class SpringIdolApplication {

	public static void main(String[] args) {

		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"ru/omsu/imit/springidol/spring-idol.xml");

		Performer john = (Performer) ctx.getBean("john");

		try {

			john.perform();

		} catch (PerformanceException e) {

			e.printStackTrace();

		}

	}

}
