package ru.omsu.imit.springidol.performers;

import ru.omsu.imit.springidol.instruments.Instrument;

import java.util.Map;

public class OneManBand implements Performer {

    private Map<String, Instrument> instruments;

    public OneManBand(){ }

    public void setInstruments(Map<String, Instrument> instruments){

        this.instruments = instruments;

    }

    public void perform() {

        for (String key : instruments.keySet()){

            System.out.println(key + " : ");
            instruments.get(key).play();

        }

    }

}
