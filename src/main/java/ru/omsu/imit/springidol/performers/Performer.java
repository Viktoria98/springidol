package ru.omsu.imit.springidol.performers;

public interface Performer {

    void perform() throws PerformanceException;

}
