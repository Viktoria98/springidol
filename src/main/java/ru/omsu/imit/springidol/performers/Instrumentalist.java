package ru.omsu.imit.springidol.performers;

import org.springframework.beans.factory.annotation.Autowired;
import ru.omsu.imit.springidol.instruments.Instrument;

public class Instrumentalist implements Performer {

    private String song;

    private Instrument instrument;

    public Instrumentalist() { }

    public void setSong(String song) {

        this.song = song;

    }

    public String getSong() {

        return song;

    }

    @Autowired
    public void setInstrument(Instrument instrument) {

        this.instrument = instrument; // инструмента

    }

    public String screamSong() {

        return song;

    }

    public void perform() throws PerformanceException {

        System.out.print("Playing " + song + " : ");
        instrument.play();

    }



}
