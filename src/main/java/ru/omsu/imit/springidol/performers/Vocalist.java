package ru.omsu.imit.springidol.performers;

public class Vocalist implements Performer {

    private String song;

    public void setSong(String song) {

        this.song = song;
        
    }

    public void perform() throws PerformanceException {

        System.out.print("Singing " + song);

    }

}
