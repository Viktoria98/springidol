package ru.omsu.imit.springidol.performers;

import ru.omsu.imit.springidol.instruments.Instrument;

public abstract class AbstractInstrumentalist implements Performer {

    private String song;

    public AbstractInstrumentalist() { }

    public void setSong(String song) {

        this.song = song;

    }

    // Используется внедряемый метод getInstrument()
    public void perform() throws PerformanceException {

        System.out.print("Playing " + song + " : ");
        getInstrument().play();

    }

    public abstract Instrument getInstrument(); // Внедряемый метод

}