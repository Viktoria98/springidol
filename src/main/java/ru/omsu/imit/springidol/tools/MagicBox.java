package ru.omsu.imit.springidol.tools;

public interface MagicBox {

    public String getContents();

}
