package ru.omsu.imit.springidol.instruments;

public class Cymbal implements Instrument {
    public Cymbal() {}

    public void play() {
        System.out.println("CRASH CRASH CRASH");
    }
}