package ru.omsu.imit.springidol.instruments;

public class Guitar implements Instrument {
    public Guitar() {}

    public void play() {
        System.out.println("STRUM STRUM STRUM");
    }
}
