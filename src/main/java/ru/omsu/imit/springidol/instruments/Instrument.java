package ru.omsu.imit.springidol.instruments;

public interface Instrument {
    public void play();
}
